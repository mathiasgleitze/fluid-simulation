﻿open System
open BenchmarkDotNet.Attributes
open BenchmarkDotNet.Engines
open BenchmarkDotNet.Running
open System.IO
open BenchmarkDotNet.Configs
open System.Threading.Tasks
open FluidUtilities
open FluidSimulation
open LinearSolving
(*
Fluid simulation library.
A port of Troels Henriksen's version:
    https://github.com/diku-dk/futhark-benchmarks/tree/master/accelerate/fluid
    
g : The grid resolution for a grid with side lengths g
    this solution implements grids as nested arrays, and it is at
    no point checked if all of the inner arrays of a grid are the same size.
    If the arrays vary in size the code will produce unwanted behaviour.
i : Horizontal index.
j : Vertical index.
S : A grid, purpose unknown.
U : A grid of horizontal forces.
V : A grid of vertical forces.
D : A grid of densities.
*)

module Visualisation = 

    let drawDensities (ds: float32 [][]) gminustwo =
        Array2D.init (int gminustwo) (int gminustwo) (fun i j -> 
            (clamp (ds.[i + 1].[j + 1]) |> fun value -> [| value; value; value |]))
    
    let drawOneFrame u0 v0 d0 nsolversteps timestep diffusionrate viscosity gminustwo =
        let u1, v1, d1 = step (linSolve) u0 v0 d0 nsolversteps timestep diffusionrate viscosity
        let ds = drawDensities d1 gminustwo
        (ds, u1, v1, d1)
    
    let drawOneFrameRaw u0 v0 d0 nsolversteps timestep diffusionrate viscosity =
        drawOneFrame u0 v0 d0 nsolversteps timestep diffusionrate viscosity (int64 ((Array.length u0) - 2))
    
    let getEndFrame stepFunction (d: FluidData) =
        let rec getEndFrameInner nsteps1 (u1, v1, d1) =
            match nsteps1 with
            | 0 -> (u1, v1, d1)
            | _ ->
                stepFunction u1 v1 d1 d.Nsolversteps d.Timestep d.Diffusionrate d.Viscosity
                |> getEndFrameInner (nsteps1 - 1)
        getEndFrameInner d.Nsteps (d.U, d.V, d.D)
    

module Benchmarking =

    ///A simple class making it possible to easily create parameters for benchmarking
    type BenchMarkingData() =
        static member StaggeredFillerData (listOfSizes: int list) : Staggered list =
            [for g in listOfSizes do yield {
                Name="" + g.ToString();
                D=Array.init g (fun i -> Array.init g (fun j ->  float32 (i + j)));
                Nsolversteps=10;
                Timestep=0.1f;
                Diffusionrate=0.0001f}]

        static member Arr2DFillerData (listOfSizes: int list) : Arr2D list =
            [for g in listOfSizes do yield {
                Name="" + g.ToString();
                D=Array2D.init g g (fun i j ->  float32 (i + j));
                Nsolversteps=10;
                Timestep=0.1f;
                Diffusionrate=0.0001f}]

        ///This method was meant to make it possible to get benchmarking parameters
        ///from .txt files, which could be shared by the Futhark implementation. 
        ///This was however horribly slow with large grids.
        static member GetTypeFromFiles (ReadFileToType: string -> 'a) =
           let projectDirectory = Environment.CurrentDirectory.Split("FSharp").[0]
           let path =  Path.Combine(projectDirectory, "benchmarking\\datasets")
           try
               let files = Array.toList(Directory.GetFiles(path, "*.in"))
               seq { for file in files do yield ReadFileToType file}
           with
               | EX -> Seq.empty<'a>

    /// A simple benchmarker using the library benchmarkdotnet 
    //[<MemoryDiagnoser>] can be used instead of [<SimpleJob>] to collect memory data
    [<SimpleJob(RunStrategy.Monitoring)>]
    [<GroupBenchmarksBy(BenchmarkLogicalGroupRule.ByCategory)>]
    type ArrayImplementedBench() =
        static member BenchParams = BenchMarkingData.StaggeredFillerData [1000; 2000; 4000; 8000]

        [<BenchmarkCategory("Single core"); Benchmark(Baseline = true)>]
        [<ArgumentsSource(nameof ArrayImplementedBench.BenchParams)>]
        member self.unaware(d: Staggered) = diffuse (linSolve) 0 d.Nsolversteps d.Diffusionrate d.Timestep  d.D 
            
        [<BenchmarkCategory("Single core"); Benchmark>]
        [<ArgumentsSource(nameof ArrayImplementedBench.BenchParams)>]
        member self.aware(d: Staggered) = diffuse (linSolveAware 100) 0 d.Nsolversteps d.Diffusionrate d.Timestep  d.D 
        
        [<BenchmarkCategory("Single core"); Benchmark>]
        [<ArgumentsSource(nameof ArrayImplementedBench.BenchParams)>]
        member self.oblivious(d: Staggered) = diffuse (linSolveOblivious 100) 0 d.Nsolversteps d.Diffusionrate d.Timestep  d.D 



        [<BenchmarkCategory("4 Threads"); Benchmark(Baseline = true)>]
        [<ArgumentsSource(nameof ArrayImplementedBench.BenchParams)>]
        member self.unawareX4(d: Staggered) =
            Parallel.For(0, 3, fun _ -> diffuse (linSolve) 0 d.Nsolversteps d.Diffusionrate d.Timestep  d.D  
                                        |> ignore)
          
        [<BenchmarkCategory("4 Threads"); Benchmark>]
        [<ArgumentsSource(nameof ArrayImplementedBench.BenchParams)>]
        member self.awareX4(d: Staggered) =
            Parallel.For(0, 3, fun _ -> diffuse (linSolveAware 100) 0 d.Nsolversteps d.Diffusionrate d.Timestep  d.D 
                                                |> ignore)

        [<BenchmarkCategory("4 Threads"); Benchmark>]
        [<ArgumentsSource(nameof ArrayImplementedBench.BenchParams)>]
        member self.obliviousX4(d: Staggered) = 
            Parallel.For(0, 3, fun _ -> diffuse (linSolveOblivious 100) 0 d.Nsolversteps d.Diffusionrate d.Timestep  d.D  
                                        |> ignore)



        [<BenchmarkCategory("4 Tasks"); Benchmark(Baseline = true)>]
        [<ArgumentsSource(nameof ArrayImplementedBench.BenchParams)>]
        member self.unawareX4Tasks(d: Staggered) =
            let tasks = Array.zeroCreate 4
            for i in 0 .. 3 do
                tasks.[i] <- Task.Factory.StartNew(fun () -> 
                    diffuse (linSolve) 0 d.Nsolversteps d.Diffusionrate d.Timestep  d.D  
                    |> ignore)
            Task.WaitAll(tasks)
          
        [<BenchmarkCategory("4 Tasks"); Benchmark>]
        [<ArgumentsSource(nameof ArrayImplementedBench.BenchParams)>]
        member self.awareX4Tasks(d: Staggered) =
            let tasks = Array.zeroCreate 4
            for i in 0 .. 3 do
                tasks.[i] <- Task.Factory.StartNew(fun () -> 
                    diffuse (linSolveAware 100) 0 d.Nsolversteps d.Diffusionrate d.Timestep  d.D 
                    |> ignore)
            Task.WaitAll(tasks)

        [<BenchmarkCategory("4 Tasks"); Benchmark>]
        [<ArgumentsSource(nameof ArrayImplementedBench.BenchParams)>]
        member self.obliviousX4Tasks(d: Staggered) = 
            let tasks = Array.zeroCreate 4
            for i in 0 .. 3 do
                tasks.[i] <- Task.Factory.StartNew(fun () -> 
                    diffuse (linSolveOblivious 100) 0 d.Nsolversteps d.Diffusionrate d.Timestep  d.D 
                    |> ignore)
            Task.WaitAll(tasks)


    [<SimpleJob(RunStrategy.Monitoring)>]
    type TensorImplemented() =
        static member BenchParams = BenchMarkingData.Arr2DFillerData [1000; 2000; 4000; 8000]
    
        [<Benchmark(Baseline = true)>]
        [<ArgumentsSource(nameof TensorImplemented.BenchParams)>]
        member self.TensorCPU(d: Arr2D) = 
            diffuseArr2D linSolveTensorCPU 0 d.Nsolversteps d.Diffusionrate d.Timestep d.D
    
        [<Benchmark>]
        [<ArgumentsSource(nameof TensorImplemented.BenchParams)>]
        member self.GPU(d: Arr2D) = 
            diffuseArr2D linSolveTensorGPU 0 d.Nsolversteps d.Diffusionrate d.Timestep d.D


[<EntryPoint>]
let main argv =
    //This config was sometimes passed along to BenchmarkRunner to enable me to 
    //benchmark code that was not built with optimisations.
    let config = ManualConfig().WithOptions(ConfigOptions.DisableOptimizationsValidator)
                               .AddValidator(BenchmarkDotNet.Validators.JitOptimizationsValidator.DontFailOnError)
                               .AddLogger(BenchmarkDotNet.Loggers.ConsoleLogger.Default)
                               .AddColumnProvider(BenchmarkDotNet.Columns.DefaultColumnProviders.Instance)

    printfn "Parsing benchmarking data..."
    BenchmarkRunner.Run typeof<Benchmarking.ArrayImplementedBench>
    printfn "\nPress any key to continue."
    Console.ReadKey(true) |> ignore
    0