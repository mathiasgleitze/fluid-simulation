﻿module EdgeHandling

let inside i j g =
    i >= 1 && i <= g - 2 && j >= 1 && j <= g - 2

let inOutSideCorner i j g =
    (i = 0 || i = g - 1) && (j = 0 || j = g - 1)

///Takes the indeces of a cell situted on the edge of a grid with size g.
let outerMostInnerCell i j g =
    match i, j with
    | 0, _ -> (1, j)                        //These could have been option types, making it a better function.      
    | i, _ when i = (g - 1) -> (g - 2, j)   //I would have done this if the project was gonna be publicly available.            
    | _, 0 -> (i, 1)
    | _, j when j = (g - 1) -> (i, g - 2)
    | _, _ -> failwith "Cell: was not situated along the edge."

type Axis =
    | Vertical
    | Horizontal

///Takes the indeces of a cell situted on the edge of a grid with size g
let getAxisOfEdgeCell i j g =
    match i, j with
    | 0, _              -> Horizontal //These could have been Axis options, making it a better function.                                       
    | i,_ when i=(g-1)  -> Horizontal //I would have done this if the project was gonna be publicly available. 
    | _, 0              -> Vertical
    | _, j when j=(g-1) -> Vertical
    | _,_               -> failwith "Cell: was not situated along the edge." 

///Takes the indeces of a any cell within a grid of size g and
///ensures that boundary conditions are handled when calling <c>mapper</c>
let inline edgeHandling i j g b aFunction = 
    if inside i j g then
        aFunction i j
    elif inOutSideCorner i j g then
        0.0f
    else
        let (i1, j1) = outerMostInnerCell i j g
        let result = aFunction i1 j1
        match getAxisOfEdgeCell i j g with
        | Horizontal -> if b = 1 then -result else result
        | Vertical -> if b = 2 then -result else result